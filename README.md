# Application suivi de contact groupe1

Création d'une application de suivi de contact

L'application se lance en local, en utilisant `run.sh`, ou avec gunicorn
(voir le fichier `Procfile`)

Des tests sont exécutés avec tox et l'application peut-être pacakagée et
publiée sous Gitlab.

Pour la partie de publication vers Heroku, il faut se créer un compte, puis

```shell
npm install heroku
nodes_modules/heroku/bin/run auth:login
nodes_modules/heroku/bin/run authorizations:create
```

On récupère ainsi une clef, qu'on utilisera comme variable dans
`https://gitlab.com/<namespace>/<project>/-/settings/ci_cd`

Si on veut tester le déploiement «localement» :

```shell
apt install gitlab-runner docker
for STAGE in build test deploy uplod ;
do
  gitlab-runner exec docker --env HEROKU_KEY=<votre-clef> $STAGE
done
```
