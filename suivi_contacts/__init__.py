"""
Un module flask pour créer une application web
"""

import os
from flask import Flask

def create_app(test_config=None):
    # Création de l'application
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY = 'dev',
    )

    if test_config is None:
        # On charge la configuration par défaut de l'application
        app.config.from_pyfile('config.py', silent=True)
    else:
        # Sinon, on charge la configuraiton de test
        app.config.from_mapping(test_config)

    # On s'assure que les répertoires nécessaires existent
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from . import suivi
    app.register_blueprint(suivi.bp)
    app.add_url_rule('/', endpoint='index')

    return app
