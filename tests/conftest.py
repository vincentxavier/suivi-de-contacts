import pytest
from suivi_contacts import create_app


@pytest.fixture
def application():
    application = create_app()
    return application


@pytest.fixture
def client(application):
    return application.test_client()


@pytest.fixture
def runner(app):
    return app.test_cli_runner()
